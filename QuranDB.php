<?php
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
require_once('QuranRandomizer.php');


class QuranDB
{

    /** @var string  */
    private $dbTable = "";

    /** @var string */
    private $tableName = "";

    public function __construct()
    {
        global $wpdb;

        $this->rand = new QuranRandomizer();

        $this->tableName = $wpdb->prefix . "quranADay";
        $this->dbTable = DB_NAME . "." .$this->tableName;

        $this->createTableIfNotExist();
    }

    public function getQuote()
    {
        global $wpdb;

        $sura = $this->rand->getRandomSura();
        $ayat = $this->rand->getRandomAyat($sura);
        $sql = "SELECT * FROM " .$this->tableName. " WHERE sura = " .$sura. " and ayat =  " .$ayat. ";";
        $result = $wpdb->get_row($sql, ARRAY_A);

        $result['suraName'] = $this->rand->getSuraName($sura);

        return $result;
    }

    private function createTableIfNotExist()
    {
        global $wpdb;

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE " . $this->dbTable. "(
                  id int(11) NOT NULL AUTO_INCREMENT,
                  sura tinyint(4) DEFAULT NULL,
                  ayat int(8) DEFAULT NULL,
                  text text,
                  PRIMARY KEY (id)
                ) $charset_collate;";


        $wpdb->get_var("SHOW TABLES LIKE '". $this->tableName . "'");
        if($wpdb->num_rows != 1) {
            dbDelta( $sql );
            $this->importCsv();
        }

    }

    private function importCsv()
    {
        global $wpdb;

        if (($handle = fopen(plugin_dir_path( __FILE__ ) . "sahih.int.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, "|")) !== FALSE) {
                $sql = "INSERT INTO " . $this->tableName . "  (sura, ayat, text) values ('" .$data[0]. "', '" .$data[1]. "', '" .esc_sql($data[2]). "');";
//                echo $sql . "</br>";
                $wpdb->query($sql);
            }
        } else {
            echo 'no file';
        }
    }
}
