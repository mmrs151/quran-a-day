<?php
/*
Plugin Name: Quran verse a day
Version: 1.0
Plugin URI: https://wordpress.org/plugins/quran-verse-a-day/
Description: Display one random verse from the quran in your widget area
Author: mmrs151
Author URI: http://mmrs151.tumblr.com/
*/

require_once('QuranDB.php');

class QuranADayWidget extends WP_Widget
{
    /** @var QuranDB */
    private $quranDb;

    public function __construct()
    {
        $this->quranDb = new QuranDB();
        $this->add_stylesheet();
        $widget_details = array(
            'className' => 'QuranADayWidget',
            'description' => 'Display one random verse from the quran in your widget area'
        );
        parent::__construct('QuranADayWidget', 'Quran verse a day', $widget_details);
    }

    public function form($instance)
    {
        ?>
        <div xmlns="http://www.w3.org/1999/html">

            <span></br></br>

            </span>
        </div>

        <div class='mfc-text'>
        </div>

        <?php

        echo $args['after_widget'];
        echo "</br><a href='http://edgwareict.org.uk/' target='_blank'>Support EICT</a></br></br>";
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];

        $quote = $this->quranDb->getQuote();
        echo "<blockquote>".$quote['text'] .
             " <a target='_new' href='http://quran.com/" .
             $quote['sura'].
             "/" .
             $quote['ayat'].
             "'></br>" .
             $quote['suraName'].
             ": " .
             $quote['ayat'].
             "</a></blockquote>";

        echo $args['after_widget'];
    }

    public function update( $new_instance, $old_instance ) {
        return $new_instance;
    }

    private function add_stylesheet() {
        wp_register_style( 'prayer-time-style', plugins_url('styles.css', __FILE__) );
        wp_enqueue_style( 'prayer-time-style' );
    }
}

add_action('widgets_init', 'init_quran_a_day_widget');
function init_quran_a_day_widget()
{
    register_widget('QuranADayWidget');
}

register_deactivation_hook( __FILE__, 'uninstallQuranADay' );
function uninstallQuranADay()
{
    global $wpdb;
    $table = $wpdb->prefix . "quranADay";

    delete_option('asrSelect');

    $wpdb->query("DROP TABLE IF EXISTS $table");
}
