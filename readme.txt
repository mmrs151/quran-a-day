=== Quran verse a day ===
Contributors: mmrs151
Donate link: http://edgwareict.org.uk/
Tags: quran, verser, ayat, islam, muslim
Requires at least: 3.5
Tested up to: 4.3
Stable tag: 1.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Display a random verse from the Holy Quran.

== Description ==
Alhamdulillah.

This plugin allows you to display a random verse from the Holy Quran. It is using the Sahih International translation.
You can click on the Sura name and number to go to quran.com to see further details.


= Features =
Once the installation above is done, this will allow you to

- display a random verse from the Quran

= Upcoming features =
- Select other translations
- anything the user may ask

= shortcodes =
coming soon ...

== Installation ==
1. Download the plugin
2. Simply go under the Plugins page, then click on Add new and select the plugin's .zip file
3. Alternatively you can extract the contents of the zip file directly to your wp-content/plugins/ folder
4. Finally, just go under Plugins and activate the plugin

== Screenshots ==
1. Widget setup
2. Vertical widget
3. Horizontal widget

== Frequently Asked Questions ==
= What translation does this plugin uses? =
Sahih International translation
